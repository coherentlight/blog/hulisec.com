---
title: "Reasonably Secure Self-Custody"
author: "yuanti@hulisec.com"
date: 2021-09-15
tags: ["personal-infrastructure", "self-custody"]
---
In this post we present some guidelines for hodling your coins safely.

## Use multiple wallets
If you have any non-trivial amount of coins, split them across multiple wallets.

This protects against some human errors and we've found that empirically it
greatly increases peace-of-mind for most people.

## Have at least two backups
We recommend having your private key instantiated across two hardware wallets
(ideally by different manufacturers) and one steel plate.

The steel plate backup is for maximum durability, the second hardware wallet
backup is so that you can react immediately if you lose your first hardware
wallet and don't have to buy and initialize a replacement first.

Another standard setup is one wallet, one SD card, and one steel plate.

We currently recommend ColdCard for Bitcoin, BitBox for Ethereum, and Trezor for
Altcoins.

{{< figure src="/img/safu.gif" >}}

## Always encrypt your private key
Your private key should be protected by a passphrase.

This is absolutely essential since otherwise simple visual inspection of your
steel plate compromises your key.

You should also set up a sub-account (derived via a second passphrase) which
contains a small amount of coins to be released in cases of physical coercion.

## Store your backups in separate locations
Generally, the more separate, the better (different countries)!

A typical arrangement would be:
* primary hardware wallet in your home office
* secondary hardware wallet in your lakehouse (a trade-off between degree of
  separation and accessibility if your primary wallet is lost)
* steel plate in a lockbox in Dubai

## Known-good wallets
If you're worried about supply-chain attacks on your hardware wallet, one simple
protocol to increase trust is as follows:

1. load a fraction of your coins onto the hardware wallet
2. wait for time to pass
3. if your coins are still there, send more

Ideally, you want to select UTXOs such that a blockchain-inspecting adversary
will be led to believe that you've sent all of your coins in step 1.

## Final Comments
If you rarely access your coins, you can primarily rely on the key passphrase (8
words chosen at random from a large dictionary) and pick something easy to
remember for the device pin.

If you access your coins often, you don't want to be typing your passphrase a
lot, since every time you enter it, there's a chance that it will be
compromised, which will immediately compromise your steel plate.

In this case we recommend storing your passphrase encrypted on an SD card
(you'll need to get a wallet which supports this) and using a very strong device
PIN (12 digits+).

You must set a daily reminder to mentally check that you remember your
passphrase. If possible, backup your passphrase by storing it in the brain of
another person that you trust as well.
